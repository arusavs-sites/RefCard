/*
    StaticEngine - simple static site generator
    Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

object Strings{
    const val title = "RefCard - Справочные карты"
    const val description = "Сайт с коллекцией полезных справочных карт. Site with useful collection of reference cards"
    const val keywords = "reference, refcard, reference card, справочные карты"
    const val author = "Alex Arus (ArusAvS@gmail.com)"
}
