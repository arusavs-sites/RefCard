package site

import template.html.HtmlTemplate

object `404`: HtmlTemplate("404.html", "", "Страница не найдена - Page not found") {
    override fun getArticle() = """
            <article>
                <p>Запрашиваемая страница не найдена</p>
            </article>
        """
}