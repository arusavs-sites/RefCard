/*
    StaticEngine - simple static site generator
    Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package site.pages

import template.html.HtmlTemplate

object About: HtmlTemplate("about.html", "pages", "О сайте - About") {
    override fun getArticle() = """
            <article>
                <h2>Копирование и использование материалов сайте</h2>
                <p>Все ресурсы данного сайта защищены российскими и международными законами и соглашениями об охране авторских прав и интеллектуальной собственности (см. статьи 1259 и 1260 главы 70 "Авторское право" Гражданского Кодекса Российской Федерации от 18 декабря 2006 года N 230-ФЗ).
                Вы можете копировать и использовать материалы сайта для любых целей, кроме целей нарушающих законодательство РФ, при условии размещения ссылки на источник информации.
                Ссылка на источник должна представлять из себя активную гиперссылку на данный сайт (на главную страницу или полный адрес страницы сайта с которой взят материал).
                Запрещается запрет индексации гиперссылки поисковыми системами (с помощью "noindex", "nofollow" или любыми другими способами), а также сокрытие видимости данной ссылки от пользователей.
                В случае использования материалов сайта в программных продуктах отличных от онлайн сайтов сети Интернет разрешается указание ссылки на данный сайт в описании программы.</p>

                <p>Автор сайта и правообладатель: Алекс Арус (Alex Arus).
                Автор сайта и правообладатель не несет ответственности за возможные последствия использования размещенной на нем информации в целях, запрещенных действующим международным и российским законодательством.
                Посещая сайт Вы обязуетесь не применять полученную на нем информацию в целях, запрещённых действующим законодательством.
                На сайте используются ссылки на сторонние сайты, не принадлежащие автору сайта, доступность и содержание которых не зависят от автора данного сайта.
                Автор данного сайта оставляет за собой право вносить изменения и дополнения в текущие правила.</p>

                <h2>Лицензия и исходный код сайта</h2>
                <p>Исходный код сайта распространяется под лицензией GPLv2 и находится по адресу <a href='https://github.com/AlexArus/Site-RefCard'>https://github.com/AlexArus/Site-RefCard</a></p>

                <p>Мы используем:
                    <ul>
                        <li>иконки <a href='http://www.fatcow.com/free-icons'>FatCow-Farm Fresh Icons (CCA 3.0)</a></li>
                    </ul>
                </p>

            </article>
        """
}